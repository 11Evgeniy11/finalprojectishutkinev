import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static io.restassured.RestAssured.given;

public class MyPostTest extends AbstractTest {
    @Test
    void MyPost1() {
        JsonPath response = given()
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "1")
                .when()
                .header("X-Auth-Token", LoginTest.getMyToken())
                .get(getBaseUrl())
                .then()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .jsonPath();

        Assertions.assertEquals(response.getList("data").size(), 4);
        Assertions.assertEquals(response.get("meta.prevPage").toString(), "1");
        Assertions.assertEquals(response.get("meta.prevPage").toString(), "1");

    }

    @Test
    void MyPost2() {
        JsonPath response = given()
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .queryParam("page", "1")
                .when()
                .header("X-Auth-Token", LoginTest.getMyToken())
                .get(getBaseUrl())
                .then()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .jsonPath();

        Assertions.assertEquals(response.getList("data").size(), 4);
        Assertions.assertEquals(response.get("meta.prevPage").toString(), "1");
        Assertions.assertEquals(response.get("meta.prevPage").toString(), "1");

    }

    @Test
    void MyPost3() {
        JsonPath response = given()
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "2")
                .when()
                .header("X-Auth-Token", LoginTest.getMyToken())
                .get(getBaseUrl())
                .then()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .jsonPath();

        Assertions.assertEquals(response.getList("data").size(), 3);
        Assertions.assertEquals(response.get("meta.prevPage").toString(), "1");
        Assertions.assertEquals(response.get("meta.prevPage").toString(), "1");

    }

    @Test
    void MyPost4() {
        JsonPath response = given()
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "2")
                .when()
                .get(getBaseUrl())
                .then()
                .statusCode(401)
                .extract()
                .response()
                .body()
                .jsonPath();

    }

    @Test
    void MyPost5() {
        JsonPath response = given()
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "1")
                .when()
                .header("X-Auth-Token", LoginTest.getMyToken())
                .get(getBaseUrl())
                .then()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .jsonPath();

        Assertions.assertEquals(response.getList("data").size(), 10);
        Assertions.assertEquals(response.get("meta.prevPage").toString(), "1");
        Assertions.assertEquals(response.get("meta.prevPage").toString(), "1");

    }
}
