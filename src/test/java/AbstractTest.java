import org.junit.jupiter.api.BeforeAll;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AbstractTest {
    static Properties prop = new Properties();
    private static InputStream configFile;
    private static String username;
    private static String password;
    private static String LoginUrl;
    private static String BaseUrl;

    @BeforeAll
    static void initTest() throws IOException {
        configFile = new FileInputStream("src/main/resources/my.properties");
        prop.load(configFile);

        username =  prop.getProperty("username");
        password =  prop.getProperty("password");
        LoginUrl= prop.getProperty("LoginUrl");
        BaseUrl= prop.getProperty("baseUrl");
    }

    public static String getUsername() {
        return username;
    }
    public static String getPassword() {
        return password;
    }

    public static String getLoginUrl() {
        return LoginUrl;
    }

    public static String getBaseUrl() {
        return BaseUrl;
    }
}
