import org.junit.jupiter.api.Test;
import static io.restassured.RestAssured.given;


public class LoginTest extends AbstractTest{
    public static String getMyToken() {
        String token = given()
                .contentType("application/x-www-form-urlencoded")
                .formParam("username", getUsername())
                .formParam("password", getPassword())
                .when()
                .post(getLoginUrl())
                .then()
                .extract()
                .jsonPath()
                .get("token")
                .toString();
        return token;
    }

    @Test
     void valid() {
        given()
                .contentType("application/x-www-form-urlencoded")
                .formParam("username", getUsername())
                .formParam("password", getPassword())
                .when()
                .post(getLoginUrl())
                .then()
                .statusCode(200);
    }
    @Test
    void inValidUsername() {
        given()
                .contentType("application/x-www-form-urlencoded")
                .formParam("username", "12345")
                .formParam("password", getPassword())
                .when()
                .post(getLoginUrl())
                .then()
                .statusCode(401);
    }
    @Test
    void inValidPassword() {
        given()
                .contentType("application/x-www-form-urlencoded")
                .formParam("username", getUsername())
                .formParam("password", "123")
                .when()
                .post(getLoginUrl())
                .then()
                .statusCode(401);
    }
}
