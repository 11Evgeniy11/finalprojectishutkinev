import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static io.restassured.RestAssured.given;


public class GetOtherPostTest extends AbstractTest {
    @Test
    void NotMe1 () {
        JsonPath response = given()
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "1")
                .when()
                .header("X-Auth-Token", LoginTest.getMyToken())
                .get(getBaseUrl())
                .then()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .jsonPath();

        Assertions.assertEquals(response.getList("data").size(),4);
        Assertions.assertEquals(response.get("meta.prevPage").toString(), "1");
        Assertions.assertEquals(response.get("meta.prevPage").toString(), "1");

    }

    @Test
    void NotMe2 () {
        JsonPath response = given()
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .queryParam("page", "1")
                .when()
                .header("X-Auth-Token", LoginTest.getMyToken())
                .get(getBaseUrl())
                .then()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .jsonPath();

        Assertions.assertEquals(response.getList("data").size(),4);
        Assertions.assertEquals(response.get("meta.prevPage").toString(), "1");
        Assertions.assertEquals(response.get("meta.prevPage").toString(), "1");

    }

    @Test
    void NotMe3 () {
        JsonPath response = given()
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ALL")
                .queryParam("page", "1")
                .when()
                .header("X-Auth-Token", LoginTest.getMyToken())
                .get(getBaseUrl())
                .then()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .jsonPath();

        Assertions.assertEquals(response.getList("data").size(),4);
        Assertions.assertEquals(response.get("meta.prevPage").toString(), "1");
        Assertions.assertEquals(response.get("meta.prevPage").toString(), "1");

    }

    @Test
    void NotMe4 () {
        JsonPath response = given()
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "3")
                .when()
                .header("X-Auth-Token", LoginTest.getMyToken())
                .get(getBaseUrl())
                .then()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .jsonPath();

        Assertions.assertEquals(response.getList("data").size(),4);
        Assertions.assertEquals(response.get("meta.prevPage").toString(), "2");
        Assertions.assertEquals(response.get("meta.nextPage").toString(), "4");

    }

    @Test
    void NotMe5 () {
        JsonPath response = given()
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .queryParam("page", "10")
                .when()
                .header("X-Auth-Token", LoginTest.getMyToken())
                .get(getBaseUrl())
                .then()
                .statusCode(200)
                .extract()
                .response()
                .body()
                .jsonPath();

        Assertions.assertEquals(response.getList("data").size(),4);
        Assertions.assertEquals(response.get("meta.prevPage").toString(), "9");
        Assertions.assertEquals(response.get("meta.nextPage").toString(), "11");

    }
}
